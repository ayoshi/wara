# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Yoshinori Arai <kumagusu08@gmail.com>
pkgname=gdb
pkgver=7.12.1
pkgrel=0
pkgdesc="The GNU Debugger"
url="http://sources.redhat.com/gdb/"
arch="all"
license="GPL3"
depends=
makedepends="ncurses-dev expat-dev texinfo autoconf automake
	libtool linux-headers"
subpackages="$pkgname-doc"
source="http://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	s390x-use-elf-gdb_fpregset_t.patch
	ppc-musl.patch"

_builddir="$srcdir"/$pkgname-$pkgver
#prepare() {
#	cd "$_builddir"
#	for i in $source; do
#		case $i in
#		*.patch)
#			msg "Applying $i"
#			patch -p1 -i "$srcdir"/$i || return 1
#			;;
#		esac
#	done
#}

build () {
	cd "$_builddir"
	local _config="
		--build=$CBUILD
		--host=$CHOST
		--prefix=/usr
		--target=$CTARGET
		--disable-nls
		--disable-werror
		--disable-tls
		--disable-sim
		--disable-tui
		--enable-gdbserver=no
		--with-system-zlib=no
		--mandir=/usr/share/man
		--infodir=/usr/share/info
		--enable-static
		--disable-shared"
		
	./configure $_config || return 1
	(cd opcodes && ./configure $_config) || return 1
	make || return 1
}

package() {
	cd "$_builddir"
	make DESTDIR="$pkgdir" install-gdb || return 1

	# resolve conflict with binutils-doc
	rm -f "$pkgdir"/usr/share/info/bfd.info
	rm -f "$pkgdir"/usr/share/info/dir

	# those are provided by binutils
	rm -rf "$pkgdir"/usr/include
	rm -rf "$pkgdir"/usr/lib

	rm -r "$pkgdir"/usr/share/gdb/system-gdbinit
	
	install -Dm644 README "$pkgdir"/usr/share/doc/gdb/README
	install -m644 ChangeLog "$pkgdir"/usr/share/doc/gdb/ChangLog
	install -m644 MAINTAINERS "$pkgdir"/usr/share/doc/gdb/MAINTAINERS
	for f in COPYING COPYING.LIB COPYING3 COPYING3.LIB
	do
		install -m644 $f "$pkgdir"/usr/share/doc/gdb/$f
	done
}

sha512sums="0ac8d0a495103611ef41167a08313a010dce6ca4c6d827cbe8558a0c1a1a8a6bfa53f1b7704251289cababbfaaf9e075550cdf741a54d6cd9ca3433d910efcd8  gdb-7.12.1.tar.xz
c3872eb51b3a42c5a33f8b7542c37fab7b0548560202e5eda740a2176cdfadff9bf73c6d26bceb225829dcb509c823acae2ccc796237ac97ebe552b82582bdf5  s390x-use-elf-gdb_fpregset_t.patch
04911f87904b62dd7662435f9182b20485afb29ddb3d6398a9d31fef13495f7b70639c77fdae3a40e2775e270d7cd40d0cfd7ddf832372b506808d33c8301e01  ppc-musl.patch"
