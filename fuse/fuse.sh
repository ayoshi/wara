#!/bin/sh
# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

MOUNTPOINT=/sys/fs/fuse/connections

start() {

	ebegin "Starting fuse"
	if ! grep -qw fuse /proc/filesystems; then
		modprobe fuse >/dev/null 2>&1 || echo $? "Error loading fuse module"
		return 1
	fi
	if grep -qw fusectl /proc/filesystems && \
	   ! grep -qw $MOUNTPOINT /proc/mounts; then
		mount -t fusectl none $MOUNTPOINT >/dev/null 2>&1 || \
			echo $? "Error mounting control filesystem"
			return 1
	fi
}

stop() {

	echo "Stopping fuse"
	if grep -qw $MOUNTPOINT /proc/mounts; then
		umount $MOUNTPOINT >/dev/null 2>&1 || \
			echo $? "Error unmounting control filesystem"
			return 1
	fi
}
