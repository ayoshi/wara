#!/bin/sh

# set the executable search path
export PATH="/bin"

# clear the screen from boot loader or early kernel messages
clear

# set the console font
setfont /share/consolefonts/Lat2-Terminus16.psfu

# mount virtual file systems
echo -n "Mounting virtual file systems ..."

mount -n -t proc proc /proc 2>/dev/null
mount -n -t sysfs sys /sys 2>/dev/null
mount -n -t devtmpfs devtmpfs /dev 2>/dev/null
mount -n -t tmpfs tmpfs /run 2>/dev/null
mount -n -t tmpfs tmp /tmp 2>/dev/null
mkdir /dev/pts
mount -n -t devpts -o "mode=620" pts /dev/pts 2>/dev/null
mkdir /run/shm /dev/shm
mount -B /run/shm /dev/shm 2>/dev/null

# if there is at least 256 MB of RAM, copy the root file system to RAM
if [ 262144 -le $(grep MemTotal: /proc/meminfo | awk "{print \$2}") ]
then
	copy=1
else
	copy=0
fi

# read the kernel command-line
home=""
for i in $(cat /proc/cmdline)
do
	case "$i" in
        	debug)
                	debug=1
                        ;;
                sleep=*)
                	interval="${i#*=}"
                         echo -n " done.
Waiting for $interval seconds ..."
			sleep $interval
                        ;;
        esac
done

# find the home partition
echo -n " done.
Searching for the home partition ..."
cd /sys/class/block
is_found=0
for i in 1 2 3 4 5 6 7 8 9 10
do
        for partition in sr*
        do
        	mount -o "ro" -t iso9660 /dev/$partition /mnt/home 2>/dev/null
                if [ 0 -eq $? ]
                then
                	if [ -f /mnt/home/boot/bzImage ] &&
                           [ -f /mnt/home/boot/initrd.xz ]
                        then
                        	is_found=1
                                break 2
                        fi
                fi
        done
        sleep 1
done
if [ 0 -eq $is_found ]
then
	echo "No found data, failure."
        exit
fi

# copy the root file system image to RAM
if [ 0 -eq $copy ]
then
	echo "No space of RAM to copy root file system."
	exit
else
	echo -n " done.
Copying the root file system to RAM ..."
	cp /mnt/home/boot/rootfs.sfs /rootfs.sfs
	umount /mnt/home
        image_path="/rootfs.sfs"
fi

#
# mount the root file system image
echo -n " done.
Mounting the root file system ..."
CHROOT_PATH="/mnt/unionfs"
losetup -r /dev/loop0 "$image_path"
mount -n -t squashfs -o "ro" /dev/loop0 "$CHROOT_PATH/root" 2>/dev/null

FUSE_OPT="-oallow_other,use_ino,suid,dev,nonempty"
UNION_OPT="-ocow,chroot=$CHROOT_PATH,max_files=32768"

# mount a layered file system
echo -n " done.
Setting up a layered file system ..."
mount -B /sys /mnt/union/sys 2>/dev/null
mount -R /dev /mnt/union/dev 2>/dev/null
mount -B /run /mnt/union/var/run 2>/dev/null
mount -B /tmp /mnt/union/tmp 2>/dev/null
mount -B /proc /mnt/union/proc 2>/dev/null

# generate unionfs
echo -n " done.
Setting up a unionfs ..."
unionfs "$FUSE_OPT" "$UNION_OPT" /rw=RW:/root=RO /mnt/union

echo "
*** Final mount status ***"
mount

# prepare to chroot
cd /mnt/union
#cd "$CHROOT_PATH"

if [ $debug ]
then
	echo "Enter debug shell"
	exec sh < /dev/console >/dev/console 2>&1
	echo
fi

echo " done."

pivot_root . /mnt
exec chroot . sh -c  'exec /sbin/sinit' </dev/console >/dev/console 2>&1
