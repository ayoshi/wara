# README #

This project wara is to develop statically linked linux distribution.

### What is this repository for? ###

* for old computer.
* Simple and differnt from major linux distro.
* Use musl libc and abuild system from alpinelinux.
* No package management but make package by yourself from source.

### How do I get set up? ###

* Install alpinelinux to your machine or using container.
* Setup installed alpinelinux to follow a setting up build environment
on HDD from wiki of http://wiki.alpinelinux.org/Developer Documentation.
* git clone https://ayoshi@bitbucket.org/ayoshi/wara.git
* edit /etc/abuild.conf.
    1. LDFLAGS="-s -static"
    2. REPODEST=$HOME/packages/
    3. PACKAGER=yourname <youremailaddress>
    4. CONFIGURE_LIBRARY_FLAGS="--enable-static --disable-shared"
* x86_64 only.
* How to run tests.
    1. make packages according to wara-iso/wara-base.packages.
        * cd packagename
        * edit APKBUILD
        * abuild -r (see abuild --help)
    2. make iso image.
        * cd wara-iso
        * edit wara-base.conf
        * just enter make
    3. use qemu or burn iso image to CDROM 
* Deployment instructions

### Contribution guidelines ###

* Try it..
* Code review and make patch or add new package.

### Who do I talk to? ###

* Repo owner or admin
